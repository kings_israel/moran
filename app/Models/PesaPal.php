<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PesaPal extends Model
{
    use HasFactory;
    protected $fillable = ['currency','user_id', 'amount', 'status', 'referenceNo', 'trackingId', 'paymentMethod',];

}
