<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderBook extends Model
{
    use HasFactory;
    protected $fillable = ['order_number','user_id', 'book_id', 'amount', 'expires_at' ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
