<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
    use \Conner\Tagging\Taggable;

    protected $fillable = [
        'isbn',
        'file_type',
        'title',
        'description',
        'published',
        'embargo_date',
        'category_id',
        'level_two_category_id',
        'level_three_category_id',
        'level_four_category_id',
        'tags',
        'author_id',
        'authors',
        'edition',
        'publication_date',
        'copyright',
        'image',
        'url',
        'language',
        'publisher',
        'price',
        'created_at',
        'updated_at',
        'book_tags',

    ];

    public function authors()
    {
        return $this->belongsTo('App\Author');
    }
    public function book_payments()
    {
        return $this->hasMany(BookPayment::class);
    }
    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }
    /**
     * Set the categories
     *
     */
    public function setCatAttribute($value)
    {
        $this->attributes['authors'] = json_encode($value);
    }
    public function getCatAttribute($value)
    {
        return $this->attributes['authors'] = json_decode($value);
    }
    public function setTagsAttribute($value)
    {
        $this->attributes['book_tags'] = json_encode($value);
    }
    public function getTagsAttribute($value)
    {
        return $this->attributes['book_tags'] = json_decode($value);
    }
    public function books()
    {
        return $this->hasMany(Book::class);
    }
}
