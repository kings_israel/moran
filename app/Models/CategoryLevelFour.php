<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryLevelFour extends Model
{
    use HasFactory;
    use \Conner\Tagging\Taggable;
    protected $fillable = [ 'sub_category', 'level_three_category_id','tags','name' ];

    public function fours()
    {
        return $this->belongsTo(CategoryLevelThree::class,'level_three_category_id');

    }
}
