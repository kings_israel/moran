<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryLevelTwo extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'sub_category',
        'category_id',
        'image',
    ];
    public function category()
    {
        return $this->belongsTo(MainCategory::class)->orderBy('created_at', 'DESC');
    }
    public function three()
    {
        return $this->hasMany(CategoryLevelThree::class);
    }

}
