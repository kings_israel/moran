<?php

namespace App\Http\Livewire;

use App\Models\Book;
use Livewire\Component;

class Wizard extends Component
{
    public $currentStep = 1;
    public $isbn, $title, $description, $file_type,$edition, $publisher, $copyright,
        $language, $price, $authors;
    public $successMsg = '';

    /**
     * Write code on Method
     */
    public function render()
    {
        return view('livewire.wizard');
    }


    /**
     * Write code on Method
     */
    public function firstStepSubmit()
    {
        $validatedData = $this->validate([
            'isbn' => 'required',
            'title' => 'required|numeric',
            'description' => 'required',
        ]);

        $this->currentStep = 2;
    }

    /**
     * Write code on Method
     */
    public function secondStepSubmit()
    {
        $validatedData = $this->validate([
            'file_type' => 'required',
            'edition' => 'required',
            'publisher' => 'required',
            'copyright' => 'required',
        ]);

        $this->currentStep = 3;
    }
    public function thirdStepSubmit()
    {
        $validatedData = $this->validate([
            'language' => 'required',
            'price' => 'required',
            'authors' => 'required',
        ]);

        $this->currentStep = 4;
    }
    /**
     * Write code on Method
     */
    public function submitForm()
    {
        Book::create([
            'name' => $this->name,
            'price' => $this->price,
            'detail' => $this->detail,
            'status' => $this->status,
        ]);

        $this->successMsg = 'Book successfully created.';

        $this->clearForm();

        $this->currentStep = 1;
    }

    /**
     * Write code on Method
     */
    public function back($step)
    {
        $this->currentStep = $step;
    }

    /**
     * Write code on Method
     */
    public function clearForm()
    {
        $this->name = '';
        $this->price = '';
        $this->detail = '';
        $this->status = 1;
    }
}
