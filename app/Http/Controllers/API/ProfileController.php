<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\LevelTwoCategoryController;
use App\Jobs\SendSms;

use App\Models\Book;
use App\Models\BookPayment;
use App\Models\CategoryLevelTwo;
use App\Models\Jambopay;
use App\Models\MainCategory;
use App\Models\Rating;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group User Profile Management
 *
 * APIs for managing user profile and phone numbers
 */
class ProfileController extends Controller
{

    /**
     * Edit User's Profile
     *
     * Change User Profile details
     * @bodyParam first_name string required Last Name.
     * @bodyParam last_name string required First Name.
     * @bodyParam email string required Email Address.
     * @authenticated
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editProfile(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email',
            ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], Response::HTTP_BAD_REQUEST);
        }
        $user = Auth::user();
        if($request->hasFile('image')){
            if (! is_dir(public_path('/images'))){
                mkdir(public_path('/images'), 0777);
            }
            $filename = $request->image->getClientOriginalName();
            $request->image->move(public_path('/images'),$filename);;
            $user->image = 'images'.$filename;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->save();
            return response()->json(['message' => "User Profile Picture Successfully Saved"], 200);
        }
        else{
            if ($user){
                $user->update([
                    'first_name'=>$request->first_name,
                    'last_name'=>$request->last_name,
                    'email'=>$request->email,
                ]);

                return response()->json(['message' => 'User Profile Updated Successfully'], Response::HTTP_OK);

            }else{
                return response()->json(['message' => false, 'comment' => 'Invalid user'], Response::HTTP_BAD_REQUEST);
            }
        }

    }
    public function jambopay(Request $request){
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|numeric|min:9',
            'book_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()
                ->json(['message' => 'Phone Number and book_id are required'], 400);
        }
        $bk = BookPayment::where('user_id', Auth::id())->where('book_id', $request->book_id)->where('status', 1)->first();
        if ($bk) {
            return response()->json(['message' => 'This Book already bought'], 400);
        }
        $amount = 6;
        return view('welcome', compact('amount'));
    }
    
    public function saveRating(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'rating' => 'required',
                'book_id' => 'required',

            ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], Response::HTTP_BAD_REQUEST);
        }
        $book = DB::table('books')
            ->where('id', $request->book_id)
            ->update([
                'rating' => $request->rating
            ]);
        if ($book){
            return response()->json(['message' => 'Book Rating Updated Successfully'], Response::HTTP_OK);
        }else{
            return response()->json(['message' => false, 'comment' => 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }
    }

    public function saveCategory(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'user_level_two' => 'required',
            ]);
        $user_level = json_encode($request->user_level_two);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], Response::HTTP_BAD_REQUEST);
        }
        $user = Auth::user();
        if ($user){
            $user->update([
                'user_level_two'=>$user_level,
            ]);
            return response()->json(['message' => 'User Level Two Categories Updated Successfully'], Response::HTTP_OK);
        }else{
            return response()->json(['message' => false, 'comment' => 'Invalid user'], Response::HTTP_BAD_REQUEST);
        }
    }
    public function rating(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'book_id' => 'required',
                'rating' => 'required',
            ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], Response::HTTP_BAD_REQUEST);
        }
        $user = Auth::user();
        if ($user){
            $jp = Rating::create([
                'user_id' => $user->id,
                'book_id' => $request->book_id,
                'rating' => $request->rating,
            ]);
            return response()->json(['message' => 'Ratings Updated Successfully'], Response::HTTP_OK);
        }else{
            return response()->json(['message' => false, 'comment' => 'Invalid user'], Response::HTTP_BAD_REQUEST);
        }
    }

    public function levelTwo(){
        $category = CategoryLevelTwo::query()
            ->orderby("id","asc")
            ->distinct()
            ->select('id','name')->get();
        if ($category){
            return response()->json(['category' => $category]);
        }
        else{
            return response()->json(['message' => 'No Category Found'], Response::HTTP_OK);

        }

    }
    public function mainCategoryBooks(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'category_id' => 'required',
            ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], Response::HTTP_BAD_REQUEST);
        }
        $book = Book::orderBy('created_at', 'DESC')->where('category_id', $request->category_id)->get();

        if ($book){
            return response()->json(['book' => $book]);
        }
        else{
            return response()->json(['error' => 'No Books Found'], Response::HTTP_OK);

        }

    }
    /**
     * Delete Account
     *
     * Deactivates user account
     * @authenticated
     */
    public function deleteAccount()
    {
        $profile = Auth::user();
        if ($profile) {
            $profile->update(['is_active' => false]);
            //delete user access_tokens
            $profile->tokens->each(function ($token){
                $token->delete();
            });
            activity()
                ->inLog('general')
                ->causedBy($profile)
                ->log($profile->first_name.' '.$profile->last_name.' deleted their profile');
            return response()->json(['message' => true, 'comment' => 'Account has been deleted'], Response::HTTP_OK);
        } else {
            return response()->json(['message' => false, 'comment' => 'Invalid user'], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function passcode($min = 1000, $max = 9999)
    {
        return mt_rand($min, $max);
    }

    public function get_books(){
        $books = Book::all();
        return response()->json([
            "message" => [
                "books" => $books,
            ]
        ],200);
    }
    public function get_level_two(){
        $att = DB::table('users')
            ->where('id', '=', 1)
            ->first();
        $userss =$att->user_level_two;
         $userz = (trim($userss, '"[]'));
        $myArray = explode(',', $userz);
        if ($myArray){
            $query = Book::select("*")
                ->whereIn('level_two_category_id', $myArray)
                ->get();
            return $query;
        }

    }
    public function levelTwoBooks(){
        $att = DB::table('users')
            ->where('id', '=', Auth::id())
            ->first();
        $userss =$att->user_level_two;
        $userz = (trim($userss, '"[]'));
        $myArray = explode(',', $userz);
        if ($userss){
            $query = CategoryLevelTwo::select("*")
                ->whereIn('id', $myArray)
                ->get();
            return response()->json([
                "message" => [
                    "level_two_names" => $query,
                ]
            ],200);
        }
        else{
            $books = Book::where('category_id', '!=', 1)->get();
            return response()->json([
                "message" => [
                    "level_two_names" => "no data",
                ]
            ],200);
        }
    }

    public function get_level_two_books(){
        $att = DB::table('users')
            ->where('id', '=', Auth::id())
            ->first();
        $userss =$att->user_level_two;
        $userz = (trim($userss, '"[]'));
        $myArray = explode(',', $userz);
        if ($userss){
            $query = Book::select("*")
                ->where('url', '!=', null)
                ->orWhere('url', '!=', '')
                ->whereIn('level_two_category_id', $myArray)
                ->get();
            return response()->json([
                "message" => [
                    "count" => $query->count(),
                    "books" => $query->all(),
                ]
            ],200);
        }
        else{
            $books = Book::where('category_id', '!=', 1)->where('url', '!=', null)->orWhere('url', '!=', '')->get();
            return response()->json([
                "message" => [
                    "books" => $books,
                ]
            ],200);
        }
    }

    public function get_user_books(){
        $books = [];
        $user = Auth::user();
        $jamboPayBooks = Jambopay::orderBy('created_at', 'DESC')->where('user_id', $user->id)->where('status', 1)->get();
        $bookPayments = BookPayment::where('user_id', $user->id)->where('status', 1)->get();

        if ($bookPayments) {
            foreach ($bookPayments as $bookPayment) {
                $bk = Book::where('id', $bookPayment->book_id)->first();
                array_push($books, $bk);
            }
        }
        if ($jamboPayBooks){
            foreach ($jamboPayBooks as $book) {
                $bk = Book::where('id',$book->book_id)->first();
                array_push($books, $bk);
            }
        }

        if (count($books) > 0) {
            return response()->json([
                "message" => [
                    "books" => $books,
                    "count" => collect($books)->count()
                ]
            ], 200);
        } else {
            return response()->json([
                "message" => [
                    "books" => "No Books found"
                ]
            ], 200);
        }
    }
    public function books(){
        $book = Book::all();
        if ($book!=null){
            $book_details = [
            'image'=> $book->image,
                'public'=> 2,
            ];
        }
        return response()->json(
            [
                'book_details' => $book_details,
            ],
            Response::HTTP_OK
        );
    }

    public function category(){
        $category= MainCategory::all();
        return response()->json([
            "message" => [
                "category" => $category,
            ]
        ],200);
    }
}
