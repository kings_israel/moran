<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\BookPayment;
use App\Models\Jambopay;
use App\Models\User;
use App\Models\UserNotification;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class JambopayController extends Controller
{
    public function accessToken()
    {
        $url = 'https://checkout.jambopay.com/Token';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZmQyZGQwODI5OGFlYWZhNzE0YzQyODVjMDI4YjgzMzZiNjI2NGJmNjM2ZGI2ZDMxN2ZiYzRhZTdkZmFiZjFmNWNmZDc3ZTgxYjY2M2E0YTQiLCJpYXQiOjE2Mzk0ODU3MzUsIm5iZiI6MTYzOTQ4NTczNSwiZXhwIjoxNjU1MjEwNTM1LCJzdWIiOiI3MSIsInNjb3BlcyI6W119.KHwywMlVgvEfhS7i2qRBjQltwv_rB9OeRlUMU4ML9e2Pa8thJHmXudeWmePycGztWsulUHZuqnaWq1GGN3WU__CjMuTKRopHyhuuEsl5T57xRV1jsKdrnCMwT_T7x7GMIbqjOyMGpIVabyRTggX1GycVEdlScaVKFmlDgH-9aVmP7DUDB7H-E4gMrGQx_v1xUTiQOZe2LdlN2fygXuXGyiCKvsy8ThV-uSBbfK5yZXnJ74OjKe9O_3Z_AnQYjV7CtQPLI0-TPqcIPoc4ihmxuSwNdLZoUZqutkrPJey6-1yKmTN3KCKCfDIBiGxNRwEtgmPv-twQO4NvtQ7rSsFQEaUM8f0EN1qP637AFBXnStWUsqTactJszZy9YNCHx2G7xkpIviNDcCv-EwO9ao3UjlGqz0czbeCud4GMOk1I57qwfZyED_LZLvJUEzAgbKpHYa-bRkN3YMA0Tk6LxWoy4KS0CAMPNfv3VGpF2MTkf_AZjtJXXxyivyFpLNd-NKpXEAvfKi6AJv7YMyu2p87HSlG_tp2n-gsUjFOw-VGmw4zxDpdz8970nIaDoWlE_RUChNW3z6bKRDbGjQkQ5WgG84bCE4K6sFiV-fzpNBej1NOykdrwb0bITdO6I4M70BG5O_ZBuioHSP5J_mHl17Fa8eXSPyRCkiPwG9KTEQIj_qg',
                'Content-Type: application/x-www-form-urlencoded',
                'Cookie: .AspNet.Cookies=Fu7W5t8i35uUUqMN_0hbhGQTlVq6fLUA019OWX1NBeiOskPja7aSn5EPvSQA-lpS4faI3IBbNTXxGENemG9Jh_gew5xdNyGP1dizlR-KPwsAotIChSIpb0Ak4daRY71XH_Dvkp7dQdTxaBSFYHvTlz5HQa5yoADkmqLP-bpPoagozDcGw7hOcRl3xTMf58NKZcKObV0IueZdarfhh_hZRxBLZ9lLy_Wgq-f46GWMo8NEUFadvD3etiOFD6qNZ5AgJNxBP3e-USmqzmxynlj-e4qmxFuT-YJAzAUn5EUR6VSA68c_LnLWODH6qNWQ-N49MSpi12YynTKHSBa1rAKxdGDGwjSZDVyWqH8Vl0m1xTENOvXbsydFtjG5feiaXLk3mezD3_hkLFllHaWp-HmZ-yKOi6nOlXNlDuyfqE7W5lSDwebLpOKfpWoj_Es_NKrGwZceSRbjjCi2ZDGrdehu4M6olYHzZzL8jL4rIkxGx_I'
            )
        );
        curl_setopt($curl, CURLOPT_POSTFIELDS,
        'username=demo%40webtribe.com&Grant_type=password&Password=Webtribe89*%26%5E&clientKey=d3m07r183',
     );

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        $access_token=json_decode($curl_response);
        curl_close($curl);
        return $access_token->access_token;

    }

    public function jambopay(Request $request)
    {
        // $prefix = "#";
        // $order_number = IdGenerator::generate(['table' => 'jambopays','field'=>'order_number', 'length' => 8, 'prefix'  =>$prefix]);
        $book = BookPayment::where('book_id', $request->book_id)->where('user_id', $request->user_id)->where('status', 1)->first();
        if ($book) {
            return response()->json(['message' => 'This Book already bought'], 400);
        }

        $bookjp = Jambopay::where('book_id', $request->book_id)->where('user_id', $request->user_id)->first();
        if ($bookjp) {
            return response()->json(['message' => 'This Book already bought'], 400);
        }

        $jp = Jambopay::create([
            'user_id' => $request->user_id,
            'Amount' => $request->Amount,
            'Order_Id' => $request->rand,
            'order_number' => 6,
            'book_id' => $request->book_id,
            'book_name' => $request->name_book,
        ]);

        if($jp){
            return response()->json(
                [
                    'success' => true,
                    'message' => $jp->Order_Id,
                    'token' => $this->accessToken(),

                ]
            );
        }

    }

    public function checkout(Request $request){
        DB::table('jambopays')
            ->where('Order_Id',$request->Order_Id)
            ->update([
                'Amount'=>$request->Amount,
                'Currency'=>$request->Currency,
                'Receipt'=>$request->Receipt,
                'Secure_Hash'=>$request->Checksum,
                'Status'=>$request->Status,
                'Timestamp'=>$request->Timestamp,
                'completed' => 1,
                'status' => 1,
                'channel' => "Jambopay",
            ]);
        $transaction = Jambopay::where('Order_Id', $request->Order_Id)->first();

        $book = Book::find($transaction->book_id);
            
        UserNotification::create([
            'user_id' => $transaction->user_id,
            'message' => 'Purchase of book '.$book->title.' was successful'
        ]);

        $user = User::find($transaction->user_id);

        activity()
            ->inLog('transactions')
            ->causedBy($user)
            ->log($user->first_name.' '.$user->last_name.' bought a book using Jambopay');

        return view('transaction-successful');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Jambopay  $jambopay
     * @return \Illuminate\Http\Response
     */
    public function show(Jambopay $jambopay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Jambopay  $jambopay
     * @return \Illuminate\Http\Response
     */
    public function edit(Jambopay $jambopay)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Jambopay  $jambopay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jambopay $jambopay)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Jambopay  $jambopay
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jambopay $jambopay)
    {
        //
    }

    public function showSuccessfulTransaction()
    {
        return view('transaction-successful');
    }

    public function cancelTransaction()
    {
        return view('cancel-transaction');
    }
}
