<?php

namespace App\Http\Controllers;
use App\Models\MainCategory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Models\Author;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Yajra\DataTables\Facades\DataTables;

class LanguageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.languages.index');
    }
    /**
     * Get Users DataTable
     *
     * @return \Illuminate\Http\Response
     */
    public function getLanguages()
    {
        $users = Language::all();
        return Datatables::of($users)

            ->addColumn('action', function ($users) {
                return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
							            <li class="nav-item edit"><a class="nav-link edit" href="#" id="'.$users->id.'"><i class="nav-icon la la-edit"></i><span class="nav-text">Edit</span></a></li>
							    		<li class="nav-item"><a class="nav-link" href="'.route('admin.languages.delete',$users->id).'"><i class="nav-icon la la-user"></i><span class="nav-text">Delete</span></a></li>

							    	</ul>
							  	</div>
							</div>
						';
            })
            ->editColumn('created_at', function ($providers){
                return Carbon::parse($providers->created_at)->format('Y-m-d h:i:s');
            })
            ->make(true);
    }
    function postLanguage(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        $error_array = array();
        $success_output = '';
        if ($validation->fails())
        {
            foreach ($validation->messages()->getMessages() as $field_name => $messages)
            {
                $error_array[] = $messages;
            }
        }
        else
        {
            if($request->get('button_action') == 'insert')
            {
                $category = new Language([
                    'name'    =>  $request->get('name'),
                ]);
                $category->save();
                $success_output = '<div class="alert alert-success">Language Create Successfully</div>';
            }
            if($request->get('button_action') == 'update')
            {
                $category = Language::find($request->get('id'));
                $category->name = $request->get('name');
                $category->save();
                $success_output = '<div class="alert alert-success">Language Updated Successful</div>';
            }

        }
        $output = array(
            'error'     =>  $error_array,
            'success'   =>  $success_output
        );
        echo json_encode($output);
    }
    function fetchdatas(Request $request)
    {
        $id = $request->input('id');
        $category = Language::find($id);
        $output = array(
            'name'    =>  $category->name,
        );
        echo json_encode($output);
    }

    public function mainLanguage(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput()->with('error', $validator->errors()->first());
        }
        $form_data = array(
            'name' => $request->name,
        );
        $cate = Language::create($form_data);
        if ($cate){
            return redirect()->route('admin.languages.index')->with('message','Languages created Successfully');
        }
        else{
            return redirect()->route('admin.languages.index')->with('error','Something went wrong..Try again');
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function show(Language $language)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function edit(Language $language)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Language $language)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function destroy(Language $language)
    {
        //
    }
}
