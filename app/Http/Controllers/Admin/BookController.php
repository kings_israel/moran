<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Author;
use App\Models\Book;
use App\Models\BookPayment;
use App\Models\CategoryLevelThree;
use App\Models\CategoryLevelTwo;
use App\Models\Language;
use App\Models\MainCategory;
use App\Models\PriceList;
use App\Models\Publisher;
use Database\Seeders\LevelTwoCategorySeeder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.books.index');
    }
    /**
     * Get Users DataTable
     *
     * @return \Illuminate\Http\Response
     */
    public function getBooks()
    {
        $books = Book::orderBy('created_at', 'DESC');
        return Datatables::of($books)
            ->addIndexColumn()

//            ->addColumn('active', function ($users){
//                if ($users->active == true) {
//                    return '<span class="label label-lg font-weight-bold label-light-success label-inline">Active</span>';
//                }else {
//                    return '<span class="label label-lg font-weight-bold label-light-warning label-inline">Suspended</span>';
//                }
//            })
//            ->addColumn('tags', function ($books){
//                $tags = [];
//                foreach ($books->tags as $tag) {
//                    array_push($tags, $tag->name);
//                }
//                //return '<span class="label label-lg font-weight-bold label-light-success label-inline">Active</span>';
//
//                return  implode(", ", $tags);
//            })
            ->addColumn('action', function ($books) {
                return '<div class="dropdown dropdown-inline">
								<a href="" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">
	                                <i class="la la-cog"></i>
	                            </a>
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<ul class="nav nav-hoverable flex-column">
							    		<li class="nav-item"><a class="nav-link" href="'.route('admin.catalogue-books.edit',Crypt::encrypt($books->id)).'"><i class="nav-icon la la-edit"></i><span class="nav-text">Edit</span></a></li>
							    		<li class="nav-item"><a class="nav-link" href="'.route('admin.book.delete',$books->id).'"><i class="nav-icon la la-edit"></i><span class="nav-text">Delete</span></a></li>
							    	</ul>
							  	</div>
							</div>

						';
            })
            ->rawColumns(['action'])

            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $author = Author::all();
        $main_category = MainCategory::query()->orderby("id","asc")->select('id','name')->get();
        $authors = Author::query()->orderby("id","asc")->select('id','full_name')->get();
        $language = Language::query()->orderby("id","asc")->select('id','name')->get();
        $publisher = Publisher::query()->orderby("id","asc")->select('id','name')->get();

        return view('admin.books.create',compact('authors','main_category','language','publisher'));
    }
    public function getCategory($category_id)
    {
        $empData['data'] = CategoryLevelTwo::orderby("id","asc")
        ->select('id','name')
        ->where('category_id',$category_id)
        ->get();
        return response()->json($empData);
//        $data = CategoryLevelTwo::where('category_id',$category_id)->get();
//        \Log::info($data);
//        return response()->json(['data' => $data]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
     // $data = $request->all();
       //dd($data);
        $validator = Validator::make($request->all(), [
            'isbn' => 'required',
            'title' => 'required',
            'publisher' => 'required',
            'language' => 'required',
            'authors' => 'required',
            'category_id' => 'required',
            'edition' => 'required',
            'copyright' => 'required',
            'pages' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput()->with('error', $validator->errors()->first());
        }
        if ($image = $request->file('image')) {
            $destinationPath = 'images';
            if(!is_dir($destinationPath)) {
                mkdir($destinationPath, 0755, true);
            }
            $image_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $image_name);
        }
       // $tags = explode(",", $request->tags);
        //$book_tags = explode(",", $request->tags);
        $book_tags = json_encode($request->tags);
        $authors = json_encode($request->authors);
        //book_tags
//        $form_data = array(
//            'isbn' => $request->isbn,
//            'title' => $request->title,
//            'description' => $request->description,
//            'publisher' => $request->publisher,
//            'language' => $request->language,
//            'price' => $request->price,
//            'pages'=> $request->pages,
//            'publication_date' => $request->publication_date,
//            'authors' => $authors,
//            'category_id' => $request->category_id,
//            'level_two_category_id' => $request->level_two_category_id,
//            'level_three_category_id' => $request->level_three_category_id,
//            'level_four_category_id' => $request->level_four_category_id,
//            'edition' => $request->edition,
//            'copyright' => $request->copyright,
//            'tags' =>$book_tags,
//            'image'=> $image_name,
//        );
        $handler = new Book();
        $handler->isbn = $request->isbn;
        $handler->title = $request->title;
        $handler->description = $request->description;
        $handler->publisher = $request->publisher;
        $handler->language = $request->language;
        $handler->price = $request->price;

        $handler->pages = $request->pages;
        $handler->publication_date = $request->publication_date;
        $handler->authors = $authors;
        $handler->category_id = $request->category_id;
        $handler->level_two_category_id = $request->level_two_category_id;
        $handler->level_three_category_id = $request->level_three_category_id;

        $handler->level_four_category_id = $request->level_four_category_id;
        $handler->edition = $request->edition;
        $handler->copyright = $request->copyright;
        $handler->category_id = $request->category_id;
        $handler->tags = $book_tags;
        $handler->image = $image_name;
        $handler->save();


       // return $form_data;
      //  $book = Book::create($form_data);
       // $book->tag($tags);
        return Redirect::route('admin.catalogue-books.index')->with('message','Book created Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book,$id)
    {
        $id = Crypt::decrypt($id);
        $book = Book::query()->findOrFail($id);
        $author = Author::all();
        $main_category = MainCategory::query()->orderby("id","asc")->select('id','name')->get();
        $authors = Author::query()->orderby("id","asc")->select('id','full_name')->get();
        $language = Language::query()->orderby("id","asc")->select('id','name')->get();
        $publisher = Publisher::query()->orderby("id","asc")->select('id','name')->get();

        return view('admin.books.edit',compact('book','authors','main_category','language','publisher'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'isbn' => 'required',
            'title' => 'required',
            'publisher' => 'required',
            'language' => 'required',
            // 'authors' => 'required',
            'category_id' => 'required',
            'edition' => 'required',
            'copyright' => 'required',
            'pages' => 'required',

        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput()->with('error', $validator->errors()->first());
        }
        $book_tags = json_encode($request->tags);
        $authors = json_encode($request->authors);
        $image =$request->file('image');
        $handler = Book::find($id);

        if ($image !=null) {
            $destinationPath = 'images';
            if(!is_dir($destinationPath)) {
                mkdir($destinationPath, 0755, true);
            }
            $image_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $image_name);
            $handler->isbn = $request->isbn;
            $handler->title = $request->title;
            $handler->description = $request->description;
            $handler->publisher = $request->publisher;
            $handler->language = $request->language;
            $handler->price = $request->price;

            $handler->pages = $request->pages;
            $handler->publication_date = $request->publication_date;
            $handler->authors = $authors;
            $handler->category_id = $request->category_id;
            $handler->level_two_category_id = $request->level_two_category_id;
            $handler->level_three_category_id = $request->level_three_category_id;

            $handler->level_four_category_id = $request->level_four_category_id;
            $handler->edition = $request->edition;
            $handler->copyright = $request->copyright;
            $handler->category_id = $request->category_id;
            $handler->tags = $book_tags;
            $handler->image = $image_name;
            $handler->save();

            return Redirect::route('admin.catalogue-books.index')->with('message','Book created Successfully');
        }
        else{
            $handler->isbn = $request->isbn;
            $handler->title = $request->title;
            $handler->description = $request->description;
            $handler->publisher = $request->publisher;
            $handler->language = $request->language;
            $handler->price = $request->price;

            $handler->pages = $request->pages;
            $handler->publication_date = $request->publication_date;
            $handler->authors = $authors;
            $handler->category_id = $request->category_id;
            $handler->level_two_category_id = $request->level_two_category_id;
            $handler->level_three_category_id = $request->level_three_category_id;

            $handler->level_four_category_id = $request->level_four_category_id;
            $handler->edition = $request->edition;
            $handler->copyright = $request->copyright;
            $handler->category_id = $request->category_id;
            $handler->tags = $book_tags;
            $handler->save();
            return Redirect::route('admin.catalogue-books.index')->with('message','Book created Successful');
        }

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function delete(Book $book,$id)
    {
        $book = Book::query()->findOrFail($id);
        $book->delete();
        return Redirect::route('admin.catalogue-books.index')->with('message','Book Deleted Successful');

    }
    public function deleteBook($id) {
        $post = Book::findOrFail($id);
        $post->delete();
        activity()
            ->inLog('general')
            ->causedBy(Auth::user())
            ->performedOn($post)
            ->log(Auth::user()->first_name.' '.Auth::user()->last_name.' deleted the book '.$post->title);
            
        return redirect()->back()->with('error', 'Book has been deleted successfully!');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroys(Request $request, $id)
    {
        $id = Crypt::decrypt($id);
        $book = Book::query()->findOrFail($id);
        $book->delete();

        activity()
            ->inLog('general')
            ->causedBy(Auth::user())
            ->performedOn($book)
            ->log(Auth::user()->first_name.' '.Auth::user()->last_name.' deleted the book '.$book->title);

        return Redirect::route('admin.catalogue-books.index')->with('message','Book Deleted Successful');
    }

    public function mainCategoty(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput()->with('error', $validator->errors()->first());
        }
        $form_data = array(
            'name' => $request->name,
        );
        $cate = MainCategory::create($form_data);

        activity()
            ->causedBy(Auth::user())
            ->inLog('general')
            ->log(Auth::user()->first_name.' '.Auth::user()->last_name.' added a Main Category, '.$cate->name);
            
        if ($cate){
            return redirect()->route('admin.catalogue-books.create')->with('message','Main Category created Successfully');
        }
        else{
            return redirect()->route('admin.catalogue-books.create')->with('error','Something went wrong..Try again');
        }
    }
    public function mainCategotyTwo(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'category_id' => 'required',
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput()->with('error', $validator->errors()->first());
        }
        $form_data = array(
            'name' => $request->name,
            'category_id' => $request->category_id,
        );
        $cate = CategoryLevelTwo::create($form_data);

        activity()
            ->causedBy(Auth::user())
            ->inLog('general')
            ->log(Auth::user()->first_name.' '.Auth::user()->last_name.' added a Level Two Category, '.$cate->name);
            
        if ($cate){
            return redirect()->route('admin.catalogue-books.create')->with('message','Level Two Category created Successfully');
        }
        else{
            return redirect()->route('admin.catalogue-books.create')->with('error','Something went wrong..Try again');
        }
    }
    public function mainCategotyThree(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'category_id' => 'required',
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput()->with('error', $validator->errors()->first());
        }
        $form_data = array(
            'name' => $request->name,
            'category_id' => $request->category_id,
        );
        $cate = CategoryLevelThree::create($form_data);

        activity()
            ->causedBy(Auth::user())
            ->inLog('general')
            ->log(Auth::user()->first_name.' '.Auth::user()->last_name.' added a Level Three Category, '.$cate->name);

        if ($cate){
            return redirect()->route('admin.catalogue-books.create')->with('message','Level Two Category created Successfully');
        }
        else{
            return redirect()->route('admin.catalogue-books.create')->with('error','Something went wrong..Try again');
        }
    }

    public function getMostBoughtBooks()
    {
        // $books = BookPayment::where('status', 1)->take(10)->get();
        $books = DB::table('book_payments')
                        ->where('status', 1)
                        ->select('book_id', DB::raw('count(*) as total'))
                        ->groupBy('book_id')
                        ->orderBy('total', 'DESC')
                        ->take(10)
                        ->get();
        Log::info($books);
        return Datatables::of($books)
            ->addColumn('image', function ($books) {
                $book = Book::find($books->book_id);
                return $book->image;
            })
            ->addColumn('isbn', function ($books) {
                $book = Book::find($books->book_id);
                return $book->isbn;
            })
            ->addColumn('title', function ($books) {
                $book = Book::find($books->book_id);
                return $book->title;
            })
            ->addColumn('edition', function ($books) {
                $book = Book::find($books->book_id);
                return $book->edition;
            })
            ->addColumn('publication_date', function ($books) {
                $book = Book::find($books->book_id);
                return $book->publication_date;
            })
            ->addColumn('copyright', function ($books) {
                $book = Book::find($books->book_id);
                return $book->copyright;
            })
            ->addColumn('language', function ($books) {
                $book = Book::find($books->book_id);
                return $book->language;
            })
            ->addColumn('price', function ($books) {
                $book = Book::find($books->book_id);
                return $book->price;
            })
            ->addColumn('pages', function ($books) {
                $book = Book::find($books->book_id);
                return $book->pages;
            })

            ->make(true);
    }
}
