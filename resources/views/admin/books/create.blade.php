@extends('admin.layout.master')
@section('styles')
    <link href="{{asset('assets/css/bootstrap-datepicker.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .bootstrap-tagsinput{
            width: 100%;
            padding: 10px 20px;
            box-sizing: border-box;
            background-color: #F3F6F9;
        }
        .border-line{
            border: 1px solid #c3cad8;
        }
        select {
            height: calc(1.5em + .75rem + 2px);
            padding: .375rem 1.75rem .375rem .75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            vertical-align: middle;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }
    </style>
@endsection
@section('content')
    <!--begin::Card-->
    <div class="card card-custom card-transparent" style="margin-top: -6%;">
        <div class="card-body p-0">
            <!--begin::Wizard-->
            <div class="wizard wizard-4" id="kt_wizard" data-wizard-state="step-first" data-wizard-clickable="true">
                <!--begin::Card-->
                <div class="card card-custom card-shadowless rounded-top-0">
                    <!--begin::Body-->
                    <div class="card-body p-0">
                        <div class="row justify-content-center py-8 px-8 py-lg-15 px-lg-10">
                            <div class="col-xl-12 col-xxl-12">
                                <!--begin::Wizard Form-->
                                <form class="form" id="kt_form" method="POST" action="{{route('admin.catalogue-books.store')}}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row justify-content-center">
                                        <div class="col-xl-12">
                                            <!--begin::Wizard Step 1-->
                                            <h5 class="text-dark font-weight-bold">Add Books's Details:</h5>
                                            <div class="row">
                                                <div class="col-xl-4">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>Book EAN</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg @error('isbn') is-invalid @enderror"  value="{{old('isbn')}}" name="isbn" placeholder="Book EAN" required />
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="col-xl-4">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>Book Title</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg @error('title') is-invalid @enderror"  value="{{old('title')}}" name="title" placeholder="Book Title" required />
                                                    </div>                                                    <!--end::Input-->
                                                </div>
                                                <div class="col-xl-4">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>Publisher Name</label>
                                                        <select name="publisher" id="publisher" class="form-control form-control-lg form-control-solid category" required>
                                                            <option selected disabled value="0" >-- Select Publisher --</option>
                                                            <!-- Read Departments -->
                                                            @foreach($publisher as $main)
                                                                <option value='{{ $main->name }}'>{{ $main->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                            </div>
                                            <div class="separator separator-dashed my-5"></div>
                                            <div class="row">
                                                <div class="col-xl-7">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>Description</label>
                                                        <textarea type="text" class="form-control form-control-solid form-control-lg @error('description') is-invalid @enderror"  value="{{old('description')}}"  name="description" placeholder="Please enter the book description" rows="1"  required></textarea>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="col-xl-3">
                                                    <div class="form-group">
                                                        <label>Language</label>
                                                        <select name="language" id="language" class="form-control form-control-lg form-control-solid category" required>
                                                            <option selected disabled value="0" >-- Select Language --</option>
                                                            <!-- Read Departments -->
                                                            @foreach($language as $main)
                                                                <option value='{{ $main->name }}'>{{ $main->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xl-2">
                                                    <div class="form-group">
                                                        <label>Edition</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg @error('description') is-invalid @enderror"  value="{{old('edition')}}" name="edition" placeholder="Edition"  required />
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-xl-3">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>Copyright</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg  @error('copyright') is-invalid @enderror"  value="{{old('copyright')}}" name="copyright"  placeholder="Copyright" required />
                                                    </div>
                                                </div>
                                                <div class="col-xl-3">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>Price</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg @error('price') is-invalid @enderror"  value="{{old('price')}}" name="price" placeholder="Price"  required/>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3">
                                                    <div class="form-group">
                                                        <label>Pages</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg @error('pages') is-invalid @enderror"  value="{{old('pages')}}" name="pages" placeholder="Pages" required/>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3">
                                                    <!--begin::Select-->
                                                    <div class="form-group">
                                                        <label>Publication Date</label>
                                                        <input type="date" class="form-control form-control-solid form-control-lg @error('publication_date') is-invalid @enderror"  value="{{old('publication_date')}}" name="publication_date" placeholder="Publication Date"  required/>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                            </div>
                                            <div class="separator separator-dashed my-5"></div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>Authors</label>
                                                        <div id="main_dropdown_menu">
                                                            <select class="form-control select2" multiple data-live-search="true" name="authors[]" required style="padding: .375rem 1.75rem .375rem .75rem;">
                                                                @foreach($authors as $main)
                                                                    <option value='{{ $main->full_name }}'>{{ $main->full_name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-xl-4">
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label text-left">Cover</label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="image-input image-input-outline" id="kt_user_add_avatar">
                                                                <div class="image-input-wrapper" style="background-image: url({{asset('assets/media/users/100_6.jpg)')}}"></div>
                                                                <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change Book Cover">
                                                                    <i class="fa fa-pen icon-sm text-muted"></i>
                                                                    <input type="file" name="image" accept=".png, .jpg, .jpeg" required/>
                                                                    <input type="hidden" name="profile_avatar_remove" />
                                                                </label>
                                                                <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel Book Cover">
																							<i class="ki ki-bold-close icon-xs text-muted"></i>
																						</span>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <!--end::Input-->
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-4">
                                                    <!--begin::Input-->
                                                    <div class="form-group" style="margin-bottom: 8px!important;">
                                                        <label>Main Category</label>
                                                        <select name="category_id" id="category" class="form-control form-control-lg form-control-solid category" required>
                                                            <option selected disabled value="0" >-- Select Main Category --</option>
                                                            <!-- Read Departments -->
                                                            @foreach($main_category as $main)
                                                                <option value='{{ $main->id }}'>{{ $main->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
{{--                                                    <button type="button" class="btn btn-secondary btn-sm one" data-toggle="modal" data-target="#myModal">Add Main Category</button>--}}
                                                </div>
                                                <div class="col-xl-4">
                                                    <!--begin::Input-->
                                                    <div class="form-group" style="margin-bottom: 8px!important;">
                                                        <label>Level Two Category</label>
                                                        <select name="level_two_category_id" id="subCategory" class="form-control form-control-lg form-control-solid twos">
                                                            <option selected disabled  value='0'>-- Select Level Two Category --</option>
                                                        </select>
                                                    </div>
{{--                                                    <button type="button" class="btn btn-secondary btn-sm two" data-toggle="modal" data-target="#myModalTwo">Add Level Two Category</button>--}}

                                                </div>
                                                <div class="col-xl-4">
                                                    <!--begin::Input-->
                                                    <div class="form-group" style="margin-bottom: 8px!important;">
                                                        <label>Level Three Category</label>
                                                        <select name="level_three_category_id" id="subCategoryThree" class="form-control form-control-lg form-control-solid subCategoryThree" autocomplete="off">
                                                            <option selected disabled value='0'>-- Select Level Three Category --</option>
                                                        </select>
                                                    </div>
{{--                                                    <button type="button" class="btn btn-secondary btn-sm three" data-toggle="modal" data-target="#myModalThree">Add Level Three Category</button>--}}

                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-xl-4">
                                                    <!--begin::Input-->
                                                    <div class="form-group" style="margin-bottom: 8px!important;">
                                                        <label>Level Four Category</label>
                                                        <select name="level_four_category_id" id="subCategoryFour" class="form-control form-control-lg form-control-solid subCategoryFour">
                                                            <option selected disabled value='0'>-- Select Level Four Category --</option>
                                                        </select>
                                                    </div>
{{--                                                    <button type="button" class="btn btn-secondary btn-sm four" data-toggle="modal" data-target="#myModalFour">Add Level Four Category</button>--}}

                                                </div>
                                                <div class="col-xl-8">
                                                    <label>Book Tags</label>
                                                    <input type="text" class="form-control" data-role="tagsinput" multiple data-live-search="true" name="tags[]"style=" width: 100%;padding: 12px 20px;margin: 8px 0;box-sizing: border-box;"/>
                                                    @if ($errors->has('tags'))
                                                        <span class="text-danger">{{ $errors->first('tags') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="separator separator-dashed my-5"></div>

                                            <input class="btn btn-success font-weight-bolder border-top px-9 py-4" type="submit" value="Submit"/>
                                            <a href="{{route('admin.catalogue-books.index')}}" class="btn btn-success font-weight-bolder border-top px-9 py-4">Back</a>

                                        </div>
                                    </div>
                                </form>
                                <!--end::Wizard Form-->
                            </div>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Card-->
            </div>
            <!--end::Wizard-->
        </div>
    </div>
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" action="{{route('admin.add-main-category')}}">
                    {{ csrf_field() }}
                    @csrf
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Main Category</h4>
                    </div>
                    <div class="modal-body">
                        <span id="form_output"></span>
                        <div class="form-group">
                            <label>Main Category Name</label>
                            <input type="text" name="name" id="name" class="form-control" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id" value="" />
                        <input type="hidden" name="button_action" id="button_action" value="insert" />
                        <input type="submit" name="submit" id="action" value="Add" class="btn btn-info" />

                        <div class="form-group">
                            <button type="button" class="btn btn-light btn-elevate mr-3" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="myModalTwo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" action="{{route('admin.add-main-category-two')}}">
                    {{ csrf_field() }}
                    @csrf
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Level Two Category</h4>
                    </div>
                    <div class="modal-body">
                        <span id="form_output"></span>
                        <div class="form-group">
                            <label>Main Category</label>
                            <select name="category_id" class="form-control form-control-lg form-control-solid category" required>
                                <option selected disabled value="0" >-- Select Main Category --</option>
                                <!-- Read Departments -->
                                @foreach($main_category as $main)
                                    <option value='{{ $main->id }}'>{{ $main->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label> Level Two Category Name</label>
                            <input type="text" name="name" id="name" class="form-control" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id" value="" />
                        <input type="hidden" name="button_action" id="button_action" value="insert" />
                        <input type="submit" name="submit" id="action" value="Add" class="btn btn-info" />

                        <div class="form-group">
                            <button type="button" class="btn btn-light btn-elevate mr-3" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="myModalThree" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" action="{{route('admin.add-main-category-three')}}">
                    {{ csrf_field() }}
                    @csrf
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Level Three Category</h4>
                    </div>
                    <div class="modal-body">
                        <span id="form_output"></span>
                        <div class="form-group">
                            <label>Main Category</label>
                            <select name="category_id" class="form-control form-control-lg form-control-solid category" required>
                                <option selected disabled value="0" >-- Select Main Category --</option>
                                <!-- Read Departments -->
                                @foreach($main_category as $main)
                                    <option value='{{ $main->id }}'>{{ $main->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label> Level Three Category Name</label>
                            <input type="text" name="name" id="name" class="form-control" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id" value="" />
                        <input type="hidden" name="button_action" id="button_action" value="insert" />
                        <input type="submit" name="submit" id="action" value="Add" class="btn btn-info" />

                        <div class="form-group">
                            <button type="button" class="btn btn-light btn-elevate mr-3" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="myModalFour" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" id="student_form">
                    {{ csrf_field() }}
                    @csrf
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Level Four Category</h4>
                    </div>
                    <div class="modal-body">
                        <span id="form_output"></span>
                        <div class="form-group">
                            <label>Main Category</label>
                            <select name="category_id" class="form-control form-control-lg form-control-solid category" required>
                                <option selected disabled value="0" >-- Select Main Category --</option>
                                <!-- Read Departments -->
                                @foreach($main_category as $main)
                                    <option value='{{ $main->id }}'>{{ $main->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label> Level Four Category Name</label>
                            <input type="text" name="name" id="name" class="form-control" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id" value="" />
                        <input type="hidden" name="button_action" id="button_action" value="insert" />
                        <input type="submit" name="submit" id="action" value="Add" class="btn btn-info" />

                        <div class="form-group">
                            <button type="button" class="btn btn-light btn-elevate mr-3" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="{{asset('assets/js/pages/custom/user/add-user.js')}}"></script>
    <script>
        $(document).on('click', '.one', function(){
            $('#myModal').modal('show');
        });
    </script>
    <script>
        $(document).on('click', '.two', function(){
            $('#myModalTwo').modal('show');
        });
    </script>
    <script>
        $(document).on('click', '.three', function(){
            $('#myModalThree').modal('show');
        });
    </script>
    <script>
        $(document).on('click', '.four', function(){
            $('#myModalFour').modal('show');
        });
    </script>
    <script src="{{asset('assets/js/jquery.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            jQuery.noConflict();
            $('.category').select2();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            jQuery.noConflict();
            $('.twos').select2();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            jQuery.noConflict();
            $('.subCategoryThree').select2();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            jQuery.noConflict();
            $('.subCategoryFour').select2();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#category').change(function () {
                var id = $(this).val();
                $('#subCategory').find('option').not(':first').remove();
                $.ajax({
                    url:  APP_URL +'/admin/getSubCounty/'+id,
                    type: 'get',
                    dataType: 'json',
                    success:function (response) {
                        console.log(response)
                        var len = 0;
                        if (response.data != null) {
                            len = response.data.length;
                        }

                        if (len>0) {
                            for (var i = 0; i<len; i++) {
                                var id = response.data[i].id;
                                var name = response.data[i].name;

                                var option = "<option value='"+id+"'>"+name+"</option>";

                                $("#subCategory").append(option);
                            }
                        }
                    }
                })
            });
            $('#subCategory').change(function () {
                var id = $(this).val();
                console.log(id)
                $('#subCategoryThree').find('option').not(':first').remove();
                $.ajax({
                    url:  APP_URL +'/admin/getSubThree/'+id,
                    type: 'get',
                    dataType: 'json',
                    success:function (response) {
                        console.log(response)
                        var len = 0;
                        if (response.data != null) {
                            len = response.data.length;
                        }

                        if (len>0) {
                            for (var i = 0; i<len; i++) {
                                var id = response.data[i].id;
                                var name = response.data[i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#subCategoryThree").append(option);
                            }
                        }
                    }
                })
            });
            $('#subCategoryThree').change(function () {
                var id = $(this).val();
                console.log(id)
                $('#subCategoryFour').find('option').not(':first').remove();
                $.ajax({
                    url:  APP_URL +'/admin/getSubFour/'+id,
                    type: 'get',
                    dataType: 'json',
                    success:function (response) {
                        console.log(response)
                        var len = 0;
                        if (response.data != null) {
                            len = response.data.length;
                        }

                        if (len>0) {
                            for (var i = 0; i<len; i++) {

                                var id = response.data[i].id;
                                var name = response.data[i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#subCategoryFour").append(option);
                            }
                        }
                    }
                })
            });

        });
    </script>@endsection

