# Endpoints


## api/buy-book




> Example request:

```bash
curl -X POST \
    "http://localhost/api/buy-book" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/buy-book"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTapi-buy-book" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-buy-book"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-buy-book"></code></pre>
</div>
<div id="execution-error-POSTapi-buy-book" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-buy-book"></code></pre>
</div>
<form id="form-POSTapi-buy-book" data-method="POST" data-path="api/buy-book" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-buy-book', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-buy-book" onclick="tryItOut('POSTapi-buy-book');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-buy-book" onclick="cancelTryOut('POSTapi-buy-book');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-buy-book" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/buy-book</code></b>
</p>
</form>


## api/paypal




> Example request:

```bash
curl -X POST \
    "http://localhost/api/paypal" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/paypal"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "POST",
    headers,
}).then(response => response.json());
```


<div id="execution-results-POSTapi-paypal" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-paypal"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-paypal"></code></pre>
</div>
<div id="execution-error-POSTapi-paypal" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-paypal"></code></pre>
</div>
<form id="form-POSTapi-paypal" data-method="POST" data-path="api/paypal" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-paypal', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-paypal" onclick="tryItOut('POSTapi-paypal');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-paypal" onclick="cancelTryOut('POSTapi-paypal');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-paypal" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/paypal</code></b>
</p>
</form>



