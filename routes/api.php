<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\JambopayController;
use App\Http\Controllers\UploadController;
use App\Models\User;
use App\Http\Controllers\TransactionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\ApiMpesaController;
use App\Http\Controllers\API\ProfileController;
use App\Http\Controllers\PesaPalController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\EmailVerificationController;
use App\Http\Controllers\API\NewPasswordController;
use App\Http\Controllers\UserNotificationController;
use App\Models\Book;
use App\Models\BookPayment;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::middleware('auth:sanctum','verified')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [AuthController::class, 'logins']);
Route::post('register', [AuthController::class, 'registers']);
Route::post('logout', [AuthController::class, 'logouts'])->middleware('auth:sanctum');
Route::post('/verify', [AuthController::class, 'verify']);
Route::post('/edit-profile', [AuthController::class, 'editProfile'])->middleware('auth:sanctum');
Route::post('/add-user-level-two', [ProfileController::class, 'saveCategory'])->middleware('auth:sanctum');
Route::post('/upload-profile',  [AuthController::class, 'upload'])->middleware('auth:sanctum');


Route::post('forgot-password', [AuthController::class, 'forgotPassword']);
Route::post('send-sms', [AuthController::class, 'sendSms']);
Route::post('confirm-otp', [AuthController::class, 'submitResetPasswordForm']);
Route::post('reset-password', [AuthController::class, 'ResetPassword']);

//Route::post('/register', [AuthController::class, 'register']);
//Route::post('/login', [AuthController::class, 'login']);
//Route::post('logout', [AuthController::class, 'logout'])->middleware('auth:api');
Route::post('/buy-book', [ApiMpesaController::class, 'tests'])->middleware('auth:sanctum');
Route::post('/buy-book/test', [ApiMpesaController::class, 'tests']);
Route::get('/all-book', [ProfileController::class, 'get_level_two_books'])->middleware('auth:sanctum');
Route::post('/paypals', [PesaPalController::class, 'pesapal']);
Route::get('/level-two', [ProfileController::class, 'levelTwo']);
Route::get('/get_user_books', [ProfileController::class, 'get_user_books'])->middleware('auth:sanctum');
//get user books

Route::post('/main-category-books', [ProfileController::class, 'mainCategoryBooks']);
Route::post('/jambopay',[ProfileController::class, 'jambopay']);
Route::get('/get_user_level_two_books',[ProfileController::class, 'levelTwoBooks'])->middleware('auth:sanctum');
Route::post('/update_ratings',[ProfileController::class, 'saveRating']);


//Route::post('/try', [ApiMpesaController::class, 'stkPush']);
//Route::post('/test', [ApiMpesaController::class, 'tests']);
//Route::get('/token', [JambopayController::class, 'accessToken']);


//Route::get('/mpesa/callback', [ApiMpesaController::class, 'callback'])->name('callback');
Route::get('/category', [ProfileController::class, 'category']);
Route::get('/books', [ProfileController::class, 'books']);
Route::post('/paypal', [TransactionController::class, 'payment']);
Route::post('social/login', [AuthController::class, 'socialLogin']);

Route::post('social/login',[AuthController::class, 'oauth_login']);
Route::post('book/rating',[ProfileController::class, 'rating']);
Route::get('read/book',[UploadController::class, 'reading']);

Route::get('/user/notifications', [UserNotificationController::class, 'getUserNotifications']);
Route::get('/user/notification/{id}', [UserNotificationController::class, 'getUserNotification']);
Route::post('/user/notification/read', [UserNotificationController::class, 'markNotificationAsRead']);
Route::get('/user/notifications/read/all', [UserNotificationController::class, 'markAllNotificationsAsRead']);

Route::get('/book_payments', function() {
    $book_payments = BookPayment::where('status', true)->orderBy('transactionDate', 'DESC')->get();
    return response()->json($book_payments, 200);
});

Route::post('/successful-transaction', [JambopayController::class, 'showSuccessfulTransaction']);
Route::post('/cancel-transaction', [JambopayController::class, 'cancelTransaction']);

Route::get('books/all', function () {
    $storedBooks = Storage::disk('book')->allFiles();
    $books = [];
    foreach ($storedBooks as $storedBook) {
        array_push($books, Book::where('url', '%like%', $storedBook)->first());
    }

    return response()->json($books, 200);
});
