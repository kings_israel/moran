<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePesaPalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesa_pals', function (Blueprint $table) {
            $table->id();
            $table->string('currency');
            $table->float('amount', 10, 2);
            $table->string('status', 50);
            $table->string('referenceNo', 50);
            $table->string('trackingId', 50)->nullable();
            $table->string('paymentMethod', 50)->nullable();
            $table->string('userId', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesa_pals');
    }
}
