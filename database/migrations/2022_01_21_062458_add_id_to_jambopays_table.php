<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdToJambopaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jambopays', function (Blueprint $table) {
            $table->foreignId('users_id')->nullable()->references('id')->on('users')->onDelete('set null');
            $table->string('user_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jambopays', function (Blueprint $table) {
            $table->foreignId('users_id')->nullable()->references('id')->on('users')->onDelete('set null');
            $table->string('user_name')->nullable();

        });
    }
}
