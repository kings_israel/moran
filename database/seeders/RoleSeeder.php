<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Role::where('name', '=', 'admin')->exists() and
            !Role::where('name', '=', 'ict')->exists() and
            !Role::where('name', '=', 'finance')->exists() and
            !Role::where('name', '=', 'app_user')->exists() and
            !Role::where('name', '=', 'user')->exists())
        {
            DB::table('roles')->insert([
                ['name' => 'admin',
                    'guard_name' => 'admin',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),],
                ['name' => 'finance',
                    'guard_name' => 'admin',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),],
                ['name' => 'ict',
                    'guard_name' => 'admin',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),],
                ['name' => 'app_user',
                    'guard_name' => 'web',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),],

                ['name' => 'user',
                    'guard_name' => 'web',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),],
            ]);
        }

    }
}
