<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MpesaPaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mpesa_payments')->insert([
            [
                'user' => 'Moran Test',
                'order_number' => '299487',
                'category' => 'Educational',
                'book_name' => 'Half a Day',
                'mpesaReceiptNumber' => 'PHA7GL0J2T',
                'amount' => '500',
                'phoneNumber' => '254724073864',
                'transactionDate' =>  Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                ],
            [
                'user' => 'Moran Publishers',
                'order_number' => '29945454',
                'category' => 'Literature',
                'book_name' => 'Tom and Jerry',
                'mpesaReceiptNumber' => 'YDKPHA7GL0J2T',
                'amount' => '50',
                'phoneNumber' => '254294073864',
                'transactionDate' =>  Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'user' => 'Moran Demo',
                'order_number' => '899487',
                'category' => 'Videos and Documentaries',
                'book_name' => 'Utawala',
                'mpesaReceiptNumber' => 'PHA7SE5GL0J2T',
                'amount' => '700',
                'phoneNumber' => '25472453864',
                'transactionDate' =>  Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'user' => 'Jane Doe',
                'order_number' => '2897487',
                'category' => 'Educational',
                'book_name' => 'How to Be',
                'mpesaReceiptNumber' => 'GTSA7GL0J2T',
                'amount' => '500',
                'phoneNumber' => '2547248973864',
                'transactionDate' =>  Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'user' => 'Vivian Aluo',
                'order_number' => '45058',
                'category' => 'Literature',
                'book_name' => 'Half a Day',
                'mpesaReceiptNumber' => 'PH273THR5V',
                'amount' => '1200',
                'phoneNumber' => '254799519778',
                'transactionDate' =>  Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'user' => 'John Doe',
                'order_number' => '732396',
                'category' => 'Educational',
                'book_name' => 'Day Pass',
                'mpesaReceiptNumber' => 'PH92DWEYYA',
                'amount' => '800',
                'phoneNumber' => '254707618187',
                'transactionDate' =>  Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],


        ]);
    }
}
